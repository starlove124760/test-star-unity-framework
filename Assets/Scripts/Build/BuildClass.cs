﻿
using Star_unity_framework;
using UnityEngine;

public enum BuildExeType
{
    [ClassInstance( typeof( Build_A ) )]
    Build_A,
    [ClassInstance( typeof( Build_B ) )]
    Build_B,
    [ClassInstance( typeof( Build_C ) )]
    Build_C,
}
public interface BuildBase
{
    void OverrideScriptableObject ( ScriptableObject scriptableObject );
}
public class Build_A : BuildBase
{
    public void OverrideScriptableObject ( ScriptableObject scriptableObject )
    {
        BuildScriptableObject buildScriptableObject = (BuildScriptableObject)scriptableObject;
        buildScriptableObject.num = 10;
    }
}
public class Build_B : BuildBase
{
    public void OverrideScriptableObject ( ScriptableObject scriptableObject )
    {
        BuildScriptableObject buildScriptableObject = (BuildScriptableObject)scriptableObject;
        buildScriptableObject.num = 20;
    }
}
public class Build_C : BuildBase
{
    public void OverrideScriptableObject ( ScriptableObject scriptableObject )
    {
        BuildScriptableObject buildScriptableObject = (BuildScriptableObject)scriptableObject;
        buildScriptableObject.num = 30;
    }
}
