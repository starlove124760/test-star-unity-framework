﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using UnityEngine;
using UnityEngine.Profiling;

public enum dicEnum
{
    a = 100,
    b,
    c,
}

public class TestDictionary : MonoBehaviour
{
    Dictionary<dicEnum , int > dic1 = new Dictionary<dicEnum, int>();
    Dictionary<string , int > dic2 = new Dictionary<string, int>();
    Dictionary<class77 , int > dic3 = new Dictionary<class77, int>();
    HybridDictionary hybrid = new HybridDictionary();
    Hashtable hashtable = new Hashtable();
    class77 c77 = new class77();
    class77 c78 = new class77();
    class77 c79 = new class77();
    StringBuilder builder = new StringBuilder();
    string str = "fsjf";
    // Use this for initialization
    void Start ( )
    {
        //Value Type
        dic1.Add( dicEnum.a , 1 );
        dic1.Add( dicEnum.b , 2 );
        dic1.Add( dicEnum.c , 3 );

        //Value Type
        dic2.Add( "a" , 1 );
        dic2.Add( "b" , 2 );
        dic2.Add( "c" , 3 );

        //Refence Type
        dic3.Add( c77 , 1 );
        dic3.Add( c78 , 2 );
        dic3.Add( c79 , 3 );

        hybrid.Add( "a" , 1 );
        hybrid.Add( dicEnum.b , 2 );
        hybrid.Add( 3 , 3 );

        hashtable.Add( dicEnum.a , 1 );
        hashtable.Add( "b" , 2 );
        hashtable.Add( dicEnum.c , 3 );
    }

    // Update is called once per frame
    void Update ( )
    {
        if ( Input.GetKeyDown( KeyCode.Space ) )
        {
            #region Dic
            //GC:60B, Time:0.24ms
            Profiler.BeginSample( "Dic1 Enum" );
            int i = dic1[ dicEnum.a ];
            Profiler.EndSample();

            //GC:0B, Time:0.09ms
            Profiler.BeginSample( "Dic2 string" );
            i = dic2[ "a" ];
            Profiler.EndSample();

            //GC:0B, Time:0.09ms
            Profiler.BeginSample( "Dic3 Class77" );
            i = dic3[ c78 ];
            Profiler.EndSample();

            //GC:0B, Time:0.09ms
            Profiler.BeginSample( "Dic4 To String" );
            string TestStr = i.ToString();
            Profiler.EndSample();

            //GC:20B, Time:0.21ms
            Profiler.BeginSample( "DicHybrid Enum" );
            object o = hybrid[ dicEnum.b ];
            Profiler.EndSample();

            Profiler.BeginSample( "DicHybrid 3" );
            o = hybrid[ 3 ];
            Profiler.EndSample();

            //GC:0B, Time:0ms
            Profiler.BeginSample( "DicHybrid string" );
            i = (int)hybrid[ "a" ];
            Profiler.EndSample();

            //GC:20B, Time:0ms
            Profiler.BeginSample( "DicHashtable Enum" );
            i = (int)hashtable[ dicEnum.a ];
            Profiler.EndSample();

            //GC:0B, Time:0ms
            Profiler.BeginSample( "DicHashtable string" );
            i = (int)hashtable[ "b" ];
            Profiler.EndSample();
            #endregion

            #region boxing unboxing
            //GC:20B , Time: 0ms
            Profiler.BeginSample( "boxing 1" );
            o = i;
            int j = (int)o;
            j++;
            Profiler.EndSample();

            //GC:0B , Time: 0ms
            Profiler.BeginSample( "boxing 2" );
            c77.data = 0;
            o = c77;
            class77 c80 = (class77)o;
            c80.data = 1;
            print( c77.Equals( c80 ) );
            Profiler.EndSample();
            #endregion

            #region String Builder
            //GC 0B , Time 0.01ms
            Profiler.BeginSample( "String builder1" );
            builder.Append( str );
            string sss = builder.ToString();
            //GC 8.1KB
            Debug.Log( builder.ToString() );
            Profiler.EndSample();
            
            Profiler.BeginSample( "String builder Log" );
            //GC 9.9KB
            Debug.Log( "Build : " + builder );
            Profiler.EndSample();

            //GC 90B , Time 0.01ms
            Profiler.BeginSample( "String builder2" );
            builder.Append( "ss" );
            Profiler.EndSample();

            //GC 0B , Time 0.01ms
            Profiler.BeginSample( "String builder3" );
            string str2 = "fsnf";
            string str3 = "ss";
            builder.Append( str );
            builder.Append( str2 );
            builder.Append( str3 );
            Profiler.EndSample();

            Profiler.BeginSample( "String Test" );
            string TestStr33 = "33";

            Debug.Log( string.Format(
                "小胖 : {0} ,Tony : {1} , Eddy : {2}" , 1 , 2 , 3 ));
            Profiler.EndSample();

            #endregion
        }
    }
}

public class class77
{
    public int data = 0;
}
